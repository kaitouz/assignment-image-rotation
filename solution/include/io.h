//
// Created by kaitouz on 05/11/2021.
//


#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_H

#include "bmp.h"

bool open_file (const char* filename, FILE** file, const char* mode);
bool close_file (FILE** file);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_H
