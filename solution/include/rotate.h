//
// Created by kaitouz on 05/11/2021.
//


#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "bmp.h"

struct image rotate(struct image const source);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H


