//
// Created by kaitouz on 05/11/2021.
//

#include "bmp.h"
#include "io.h"
#include "rotate.h"

int main( int argc, char** argv ) {
    if(argc != 3) {
        fprintf(stderr, "Invalid parameters arguments; Usage: ./bmp_rotator <source_file> <destination_file>");
        return 1;
    }

    FILE *in = NULL;
    if(!open_file(argv[1], &in, "rbe")) {
        fprintf(stderr, "Failed to open input file!\n"); return 1;
    }

    FILE *out = NULL;
    if(!open_file(argv[2], &out, "wbe")) {
        fprintf(stderr, "Failed to open output file!\n"); return 1;
    }

    struct image original_image;
    enum read_status read_image_status = from_bmp(in, &original_image);
    if (read_image_status) {
        fprintf(stderr, "Read image error %u\n", read_image_status); return 1;
    }

    struct image rotated_image = rotate(original_image);
    enum write_status write_image_status = to_bmp(out, &rotated_image);
    if (write_image_status) {
        fprintf(stderr, "Write image error %u\n", write_image_status); return 1;
    }

    //free
    free(original_image.data);
    free(rotated_image.data);

    //close
    if(!close_file(&in)) {
        fprintf(stderr, "Failed to close input file!\n"); return 1;
    }

    if(!close_file(&out)) {
        fprintf(stderr, "Failed to close output file\n"); return 1;
    }

    return 0;
}

