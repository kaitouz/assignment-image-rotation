//
// Created by kaitouz on 05/11/2021.
//

#include "../include/rotate.h"

struct image rotate(struct image const source) {

    struct image ret;
    ret.width = source.height;
    ret.height = source.width;
    ret.data = malloc(source.height * source.width * sizeof(struct pixel));

    for(uint32_t row = 0; row < ret.height; row++) {
        for(uint32_t col = 0; col < ret.width; col++) {
            uint32_t source_row = source.height - col - 1;
            uint32_t source_col = row;
            *(ret.data + row * ret.width + col) = *(source.data + source_row * source.width + source_col);
        }
    }

    return ret;
}

