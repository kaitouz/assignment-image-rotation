//
// Created by kaitouz on 05/11/2021.
//


#include "../include/bmp.h"

static const uint16_t VAL_bfType = 0x4D42;
static const uint16_t VAL_biBitCount = 24;
static const uint32_t VAL_biSize = 40;
static const uint32_t DOUBLE_SIZE = 4;

static struct bmp_header create_header(struct image const* img);

enum read_status from_bmp( FILE* in, struct image* img ){
    if (!in) return READ_STREAM_NULL;
    if (!img) return READ_TARGET_NULL;

    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    fread(header, sizeof(struct bmp_header), 1, in);

    if (header->bfType > VAL_bfType || header->bfType <= 0)
        return READ_INVALID_SIGNATURE;
    if (header->biBitCount != VAL_biBitCount)
        return READ_INVALID_BITS;
    if (header->biSize != VAL_biSize)
        return READ_INVALID_HEADER;

    img->width = header->biWidth;
    img->height = header->biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));

    free(header);

    size_t padding = DOUBLE_SIZE - img->width * 3 % DOUBLE_SIZE;

    for (int row = 0; row < img->height; row++) {
        fread((char *) img->data + row * img->width * 3, sizeof(struct pixel), img->width, in);
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    if (!out) return WRITE_STREAM_NULL;
    if (!img) return WRITE_SOURCE_NULL;

   struct bmp_header header = create_header(img);
    fwrite(&header, sizeof(struct bmp_header), 1, out);

    size_t padding = DOUBLE_SIZE - img->width * 3 % DOUBLE_SIZE;

    for (int row = 0; row < img->height; row++) {
        fwrite((char *) img->data + row * img->width * 3, sizeof(struct pixel), img->width, out);
        fseek(out, padding, SEEK_CUR);
    }
    return WRITE_OK;
}

static struct bmp_header create_header(struct image const* img){
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    struct bmp_header header = {
            .bfType = VAL_bfType,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = VAL_biSize,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = VAL_biBitCount,
            .biSizeImage = height * width * sizeof(struct pixel) + (width % DOUBLE_SIZE) * height,
    };
    header.bfileSize = header.biSizeImage + sizeof(struct bmp_header);
    return header;
}

